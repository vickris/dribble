class CommentsController < ApplicationController
    before_action :find_post, only: [:create, :destroy, :edit, :update]
    
    def new
    end
    
    def create
        @comment = @post.comments.create(comment_params)
        @comment.user = current_user if current_user
        
        if @comment.save
            redirect_to @post
        else
            render :new
        end
    end
    
    def edit
        @comment = @post.comments.find(params[:id])
    end
    
    def update
        @comment = @post.comments.find(params[:id])
        if @comment.update(comment_params)
            redirect_to @post
        else
            render :edit
        end
    end
    
    def destroy
        @comment = @post.comments.find(params[:id])
        @comment.destroy
        redirect_to @post
    end
    
    private
    def comment_params
        params.require(:comment).permit(:content)
    end
    
    def find_post
        @post = Post.find(params[:post_id])
    end
end
